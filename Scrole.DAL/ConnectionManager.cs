﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Scrole.DAL
{
    public class ConnectionManager
    {
        private string _connectionString;
        public ConnectionManager()
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path);

            var root = configurationBuilder.Build();
            _connectionString = root.GetSection("ConnectionStrings").GetSection("ConnStr").Value;
            //var appSetting = root.GetSection("ApplicationSettings");
        }
        public SqlConnection GetConnection()
        {
            var con = new SqlConnection(_connectionString);
            return con;
        }
    }
}
