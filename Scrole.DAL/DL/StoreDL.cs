﻿using Scrole.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Scrole.DAL.DL
{
   public class StoreDL
    {
        QueryManager qm;
        public StoreDL()
        {
             qm = new QueryManager();
        }
        public long Manage(string filter, Store model)
        {
           
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter { ParameterName = "@Filter", Value = filter });

            var ty = model.GetType();
            var props = ty.GetProperties();

            foreach (var item in props)
            {
                bool condition = !item.GetGetMethod().IsVirtual && !item.GetGetMethod().IsCollectible;
                if (condition)
                {
                    sqlParameters.Add(new SqlParameter { ParameterName = "@" + item.Name, Value = item.GetValue(model) });
                }
            }
            return qm.ExecuteManageSP(model.ManageSp, sqlParameters);
        }

        public DataSet Get(string filter, long? Id = null, long? UserId = null)
        {
            List<SqlParameter> sqlParameters = new List<SqlParameter>();
            sqlParameters.Add(new SqlParameter { ParameterName = "@Filter", Value = filter });
            sqlParameters.Add(new SqlParameter { ParameterName = "@Id", Value = Id });
            sqlParameters.Add(new SqlParameter { ParameterName = "@UserId", Value = UserId });
            return qm.ExceuteGetSP(new Store().GetSp, sqlParameters);
        }
    }
}
