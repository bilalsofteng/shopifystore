﻿using Scrole.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Scrole.DAL.DL
{
    public interface IDL<T>  where T : BaseEntity
    {
        long Manage(T model);
        DataSet Get(string filter, long? id = null, long? pid = null, long? page = 1, int? recordsPerPage = 10);
    }
}
