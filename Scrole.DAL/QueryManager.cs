﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace Scrole.DAL
{
    public class QueryManager
    {
        private SqlConnection _conn;
        public QueryManager()
        {
            var connectionManager = new ConnectionManager();
            _conn = connectionManager.GetConnection();
        }

        public long ExecuteManageSP(string spName, List<SqlParameter> paramters)
        {
            try
            {
                _conn.Open();
                using (SqlCommand command = new SqlCommand(spName, _conn))
                {
                    //_conn.Open();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddRange(paramters.ToArray());

                    SqlParameter outPutParameter = new SqlParameter();
                    outPutParameter.ParameterName = "@Result";
                    outPutParameter.SqlDbType = System.Data.SqlDbType.BigInt;
                    outPutParameter.Direction = System.Data.ParameterDirection.Output;

                    command.Parameters.Add(outPutParameter);

                    command.ExecuteNonQuery();
                    return Convert.ToInt64(outPutParameter.Value.ToString());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
            finally
            {
                _conn.Close();
            }
        }

        public DataSet ExceuteGetSP(string spName, List<SqlParameter> paramters)
        {
            try
            {
                _conn.Open();
                using (SqlCommand command = new SqlCommand(spName, _conn))
                {
                    //_conn.Open();
                    command.CommandType = System.Data.CommandType.StoredProcedure;
                    command.Parameters.AddRange(paramters.ToArray());

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = command;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _conn.Close();
            }
        }

        public DataSet ExceuteGetCommand(string query)
        {
            try
            {
                _conn.Open();
                using (SqlCommand command = new SqlCommand(query, _conn))
                {
                    //_conn.Open();
                    command.CommandType = System.Data.CommandType.Text;

                    SqlDataAdapter da = new SqlDataAdapter();
                    da.SelectCommand = command;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                _conn.Close();
            }
        }

    }
}
