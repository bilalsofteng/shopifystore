﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Scrole.DAL.Model
{
    public class Store : BaseEntity
    {
        public Store()
        {
            this.ManageSp = "ManageStore";
            this.GetSp = "GetStore";
        }
        [Required]
        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public long UserId { get; set; }
        public string SiteUrl { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string StoreIcon { get; set; }
    }
}
