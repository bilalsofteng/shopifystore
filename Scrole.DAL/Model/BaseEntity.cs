﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Scrole.DAL.Model
{
    public class BaseEntity
    {
        public long Id { get; set; }
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public long CreatedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }

        [IgnoreDataMember]
        public virtual string ManageSp { get; set; }
        [IgnoreDataMember]
        public virtual string GetSp { get; set; }
    }
}
