import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationUtilService {

  private messages = { 
    required: 'This field is required.',
    minlength: 'This field minimum lenght should be ',
    email: 'Wrong E-Mail formate.',
    maxlength: 'This field max length should be '
   }
  constructor() { }
  public GetErrorMessages(errors: any): string {
    let errorMeassages = '';
    for (let err of Object.keys(errors)) {

      if(errorMeassages && errorMeassages.length > 0){
        errorMeassages += '<br />';
      }
      if (this.messages[err]) {
        errorMeassages += this.messages[err];
      }
      if(err == 'minlength' || err == 'maxlength'){
        errorMeassages += errors[err].requiredLength+'.';
      }
    }
    return errorMeassages;
  }
}
