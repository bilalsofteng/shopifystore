import { TestBed } from '@angular/core/testing';

import { ValidationUtilService } from './validation-util.service';

describe('ValidationUtilService', () => {
  let service: ValidationUtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ValidationUtilService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
