import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationUtilService } from 'src/app/util/validation-util.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm = this.fb.group({
    email: ['', [Validators.required, Validators.email,]],
    firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    lastName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    address: ['', [Validators.required, Validators.maxLength(500)]],
    phone:['',[Validators.required, Validators.maxLength(50)]],
    password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]]
  })
  errorMessage: string = '';

  constructor(private fb: FormBuilder, private authService: AuthService,
    private validationService: ValidationUtilService, private router: Router) { }

  ngOnInit(): void {
  }

  submit() {
    this.registerForm.markAllAsTouched();
    if (this.registerForm.valid) {
      this.authService.register(this.registerForm.value).subscribe((res) => {
        this.router.navigateByUrl('/auth/login')
      },
        (err) => {
          console.log(err);
          if(err.status == 400){
            this.errorMessage = err.error.message;
          }
        })
    }
  }

}
