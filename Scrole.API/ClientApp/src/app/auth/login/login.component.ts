import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationUtilService } from 'src/app/util/validation-util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  })

  errorMessage: string = '';
  app : any;

  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router,
    private validationService: ValidationUtilService,  private _injector: Injector) {
      this.app = this._injector.get<AppComponent>(AppComponent);
     }

  ngOnInit(): void {
  }

  submit() {
    this.loginForm.markAllAsTouched()
    if (this.loginForm.valid) {
      this.authService.login(this.loginForm.value).subscribe((res) => {
        localStorage.setItem('token', res.token);
        this.errorMessage = "";
        this.app.checkIfLogin();
        if (res.roles.indexOf('Admin') > 0) {
          this.router.navigateByUrl('\admin')
        } else {
          this.router.navigateByUrl('')
        }
      },
        (err) => {
          this.errorMessage = "Invalid Username or password!";
        })
    }
  }

}
