import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationUtilService } from 'src/app/util/validation-util.service';

@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.css']
})
export class ForgetPasswordComponent implements OnInit {

  reseEmailForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]]
  });
  resePassworForm = this.fb.group({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.maxLength(20), Validators.minLength(8)]],
    token: ['', Validators.required]
  })
  errorMessage: string;
  successMessage : string;
  showEmail : boolean = true;
  constructor(private fb: FormBuilder, private authService: AuthService, private validationService : ValidationUtilService, private router : Router) { }

  ngOnInit(): void {
  }

  submit() {
    this.reseEmailForm.markAllAsTouched();
   
    if (this.reseEmailForm.valid) {
      this.authService.resetEmailToken(this.reseEmailForm.get("email").value).subscribe((res)=>{
        this.successMessage = res;
        this.showEmail = false;
        this.errorMessage ='';
      },
      (err)=>{
        this.errorMessage = err.error.message;
      })
    }
  }

  resetPassword(){
    this.resePassworForm.markAllAsTouched();
    if(this.resePassworForm.valid){
       let password = this.resePassworForm.get('password').value;
       let email = this.resePassworForm.get('email').value;
       let token = this.resePassworForm.get('token').value;
       this.authService.resetPassword(email,password, token).subscribe((res)=>{
         alert(res);
         this.router.navigateByUrl('/auth/login');
       })
    }
  }

}
