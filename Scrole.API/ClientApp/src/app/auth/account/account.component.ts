import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IRegister } from 'src/app/models/register';
import { AuthService } from 'src/app/services/auth.service';
import { ValidationUtilService } from 'src/app/util/validation-util.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  profile: IRegister;
  changePasswordForm = this.fb.group({
    oldPassword: ['', [Validators.required]],
    newPassword: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(20)]]
  })

  accountForm =  this.fb.group({
    email: ['', [Validators.required, Validators.email,]],
    firstName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    lastName: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    address: ['', [Validators.required, Validators.maxLength(500)]],
    phone:['',[Validators.required, Validators.maxLength(50)]],
    password:['**********',[Validators.required, Validators.maxLength(50)]]
  });

  constructor(private authService: AuthService, private fb: FormBuilder, private validationService: ValidationUtilService) {
    this.authService.getProfile().subscribe((res) => {
      this.profile = res;
      this.accountForm.setValue({
        email : res.email,
        firstName: res.firstName,
        lastName : res.lastName,
        address : res.address,
        phone :res.phone,
        password : '**********'
      })
    })
  }

  ngOnInit(): void {
  }

  resetForm() {
    this.changePasswordForm.reset();
  }

  submitPassword() {
    this.changePasswordForm.markAllAsTouched();
    if (this.changePasswordForm.valid) {
      this.authService.changePassword(this.changePasswordForm.value).subscribe((res) => {
        this.resetForm();
        alert("Password Change Successfull!")
      })
    }
  }

  submit(){
    this.accountForm.markAllAsTouched();
    if(this.accountForm.valid){
      this.authService.update(this.accountForm.value).subscribe((res)=>{
      
        localStorage.setItem('token', res.token);
        alert("Profile Updated Success fully.")
      })
    }
  }

}
