import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AccountComponent } from './auth/account/account.component';
import { AuthComponent } from './auth/auth.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { CollectionsComponent } from './collections/collections.component';
import { AuthGuardService } from './services/auth/auth-guard.service';
import { StoreComponent } from './store/store.component';
import { StoresComponent } from './store/stores/stores.component';
import { ViewstoresComponent } from './viewstores/viewstores.component';

const routes: Routes = [
  { path: "", component: CollectionsComponent },
  { path: "viewstore", component: ViewstoresComponent },
  { path: "viewstore/:id", component: ViewstoresComponent },
  
  { path: "collection", component: CollectionsComponent },
  { path: "collection/:id", component: CollectionsComponent },

  {
    path: "admin", component: AdminComponent, children: [

    ]
  },
  {
    path: "store", component: StoreComponent, canActivate: [AuthGuardService], children: [
      { path: 'stores', component: StoresComponent }
    ]
  },
  {
    path: "auth", component: AuthComponent, children: [
      { path: "login", component: LoginComponent },
      { path: "register", component: RegisterComponent },
      {path:"profile", component:AccountComponent, canActivate:[AuthGuardService]},
      {path:"forgetpassword", component:ForgetPasswordComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
