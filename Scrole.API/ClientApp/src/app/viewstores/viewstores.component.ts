import { Component, Inject, OnInit } from '@angular/core';
import { IStore } from '../models/store';
import { StoreService } from '../services/store.service';
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-viewstores',
  templateUrl: './viewstores.component.html',
  styleUrls: ['./viewstores.component.css']
})
export class ViewstoresComponent implements OnInit {
  stores: Array<IStore> = [];
  apiUrl:string='';
  searchText;
  storeTypeId = 0;

  constructor(private storesService: StoreService, private rout: ActivatedRoute,
     private router: Router) {
      this.apiUrl = this.storesService.apiUrl;
   }

  ngOnInit(): void {
 
    //this.storeTypeId = +this.rout.snapshot.paramMap.get("id");
    //this.getStoreByStoreTypeId();
    this.storeTypeId = 0;
    this.rout.paramMap.subscribe(params => {
      this.storeTypeId = +params.get("id");
      if(this.storeTypeId > 0){          
        this.getStoreByStoreTypeId();
       }
       else{
        this.loadRecords();
       }

   })
   
  }
  private loadRecords() {
    this.storesService.getStores().subscribe((res) => {
      this.stores = res;

    })
  }
  private getStoreByStoreTypeId() {
    this.storesService.getStoreByStoreTypeId(this.storeTypeId).subscribe((res) => {
      this.stores = res;

    })
  }

  loadItemByStoreId(rowData: any) {
    this.router.navigate(["../collection/" + rowData.id]);
  }
}
