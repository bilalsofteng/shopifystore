import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { IStore } from 'src/app/models/store';
import { StoreService } from 'src/app/services/store.service';
import { ValidationUtilService } from 'src/app/util/validation-util.service';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.css']
})
export class StoresComponent implements OnInit {

  stores: Array<IStore> = [];
  storeFrom = this.fb.group({
    id:[0],
    title:['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
    siteUrl:['',[Validators.required]],
    description:['', [Validators.maxLength(500)]]
  });
  imageFile : any;
  imageUrl:string = "https://i.stack.imgur.com/y9DpT.jpg";

  constructor(private storesService: StoreService, private fb : FormBuilder, private validationService: ValidationUtilService) {
    this.loadRecords();
  }

  ngOnInit(): void {
  }

  private loadRecords() {
    this.storesService.getByUser().subscribe((res) => {
      this.stores = res;
    })
  }
  delete(id: number) {
    let val = confirm('Are you sure you want to delete store?');
    if (val) {
      this.storesService.delete(id).subscribe((res) => {
        if (res > 0) {
          this.loadRecords();
        }
      })
    }
  }

  resetForm(){
    this.imageUrl = "https://i.stack.imgur.com/y9DpT.jpg";
    this.storeFrom.reset();
  }
  submit(){
    this.storeFrom.markAllAsTouched();
    if(this.storeFrom.valid){
      if(this.storeFrom.value.id > 0){
        this.storesService.update(this.storeFrom.value, this.imageFile).subscribe((res)=>{
          this.resetForm();
          this.loadRecords();
        })
      }else{
        if(this.imageFile){
          alert("please upload thumnail");
          return;
        }
        this.storesService.add(this.storeFrom.value, this.imageFile).subscribe((res)=>{
          this.resetForm();
          this.loadRecords();
        })
      }
    }
  }

  openFile() {
    document.getElementById('flImg').click();
  }

  imgPreviwe(e) {
    let reader = new FileReader();
    if (e.target.files && e.target.files.length) {
      const [file] = e.target.files;
      this.imageFile = file;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.imageUrl = reader.result as string;
        // document.getElementById('img').setAttribute('src', reader.result as string)
      }
    }
  }

  openEdit(id){
    this.storesService.getById(id).subscribe((res)=>{
      this.storeFrom.setValue({
        id: res.id,
        title : res.title,
        siteUrl : res.siteUrl,
        description : res.description
      });
      this.imageUrl = this.storesService.apiUrl + res.thumbnailUrl;
      document.getElementById('btn_modal').click();
    })
  }

}
