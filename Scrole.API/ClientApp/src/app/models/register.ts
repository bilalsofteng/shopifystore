export interface IRegister {
    email: string
    password: string
    firstName: string
    lastName: string
    address : string
    phone : string
}