import { IStore } from "./store";

export interface ISProduct {
    body_html : string,
    created_at : string,
    handle : string,
    id : number,
    images : Array<any>,
    options : Array<any>,
    product_type : string,
    published_at : string,
    tags : Array<any>,
    title : string,
    updated_at : string,
    variants : Array<any>,
    vendor: string,
    price: string,
    store : IStore,
}
