export interface IBaseModel {
    id: number
    createdOn: string
    createdBy: number
    modifiedOn: string
    modifiedBy: number
    isDeleted: boolean
}
