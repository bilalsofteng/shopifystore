import { IBaseModel } from "./base-model";

export interface IStore extends IBaseModel {
    title: string
    thumbnailUrl: string
    siteUrl: string
    description: string
    isActive: boolean
    pageSize : number
    nextPage : number
    currentPage : number
    storeIcon:string
}
