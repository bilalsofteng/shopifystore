import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollectionsComponent } from './collections/collections.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminModule } from './admin/admin.module';
import { ClientModule } from './client/client.module';
import { StoreModule } from './store/store.module';
import { AuthModule } from './auth/auth.module';
import { CommonModule } from '@angular/common';
import { TokenInterceptorService } from './services/auth/token-interceptor.service';
import { JwtModule } from '@auth0/angular-jwt';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { ViewstoresComponent } from './viewstores/viewstores.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
export function tokenGetter() {
  return localStorage.getItem("access_token");
}

@NgModule({
  declarations: [
    AppComponent,
    CollectionsComponent,
    ViewstoresComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AdminModule,
    ClientModule,
    StoreModule,
    AuthModule,
    ReactiveFormsModule,
    FormsModule,
    InfiniteScrollModule, 
    Ng2SearchPipeModule,
    JwtModule.forRoot({
      config: {
       tokenGetter: tokenGetter,
      allowedDomains: ["localhost:4200"]
      },
    }),
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
