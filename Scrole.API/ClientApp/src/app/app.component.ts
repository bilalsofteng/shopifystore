import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Popover } from 'node_modules/bootstrap/dist/js/bootstrap.esm.min.js'
import { AuthService } from './services/auth.service';
import { StoreService } from './services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Scrol Digital Marketing';
  isLogin = false;
  storeTypes:any;

  constructor(private authService: AuthService, private router : Router,private storesService: StoreService) {
    this.checkIfLogin();
  }
  ngOnInit() {
    Array.from(document.querySelectorAll('button[data-bs-toggle="popover"]'))
      .forEach(popoverNode => new Popover(popoverNode))
      this.getStoreTypes();
  }

  checkIfLogin() {
    this.isLogin = this.authService.isAuthenticated();
  }

  logOut(){
    this.authService.logout();
    this.checkIfLogin();
    this.router.navigateByUrl('/');
  }
   getStoreTypes() {
    this.storesService.getStoreTypes().subscribe((res) => {
      debugger
      this.storeTypes = res;

    })
  }
  getStoreByStoreTypeId(rowData: any) {
    debugger
    this.router.navigate(["../viewstore/" + rowData.storeTypeID]);
  }

}
