import { HttpClient } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { ISProduct } from '../models/sproduct';
import { IStore } from '../models/store';
import { StoreService } from '../services/store.service';

@Component({
  selector: 'app-collections',
  templateUrl: './collections.component.html',
  styleUrls: ['./collections.component.css']
})
export class CollectionsComponent implements OnInit {
  apiUrl: string = '';
  storeUrls: Array<IStore> = []
  products: Array<ISProduct> = [];
  resutlProducts: Array<ISProduct> = [];
  storeId = 0;
  filterForm = this.fb.group({
    store: [''],
    minprice: [0],
    maxprice: [0],
  }) 

  constructor(private http: HttpClient,
     private storeService: StoreService, 
     private fb: FormBuilder,
     private rout: ActivatedRoute) {
   
    this.storeId = +this.rout.snapshot.paramMap.get("id");
    
    this.apiUrl = this.storeService.apiUrl;
    
if(this.storeId > 0){
  this.storeService.getStoreById(this.storeId).subscribe((res) => {
  
      this.storeUrls = res;
      this.setPaging(25);
      this.papulateProducts();
    });
}
else{
  this.storeService.getTrentingStores().subscribe((res) => {
    
      this.storeUrls = res;
      // if(this.storeId > 0){
      //   this.storeUrls = this.storeUrls.filter(x=>x.id == this.storeId)
      // }
      this.setPaging(25);
      this.papulateProducts();
    });
}
    


    


  }
  loadRelatedStoreItems(id:number){
    
    this.storeUrls = this.storeUrls.filter(x=>x.id == id)
    this.resutlProducts = [];
    this.products = [];
    this.storeId = 0;
    this.setPaging(25);
    this.papulateProducts();
  }

  papulateProducts() {
  
    for (let i = 0; i < this.storeUrls.length; i++) {
      this.getProducts(this.storeUrls[i]);
    }

    this.resutlProducts = this.products;
  }

  searchProducts(q) {
    if (q) {
      this.resutlProducts = this.products.filter((a) => { return a.tags.indexOf(q) > 0 || a.body_html.indexOf(q) > 0 || a.title.indexOf(q) > 0 })
    } else {
      this.resutlProducts = this.products;
    }
  }

  setStoreToFilter(id) {
    if (this.resutlProducts.length != this.products.length) {
      this.resutlProducts = this.resutlProducts.filter((a) => { return a.store.id == id })
    } else {
      this.resutlProducts = this.products.filter((a) => { return a.store.id == id })
    }
    this.filterForm.get('store').setValue(id);
  }

  setPriceFilter(minpirce, maxprice) {
    if (this.resutlProducts.length != this.products.length) {
      if (minpirce > 0) {
        this.resutlProducts = this.resutlProducts.filter((a) => {
          let prices = a.price.split('-');
          return parseFloat(prices[0]) >= parseFloat(minpirce)
        })
      }
      if (maxprice > 0) {
        this.resutlProducts = this.resutlProducts.filter((a) => {
          let prices = a.price.split('-');
          if (prices.length > 1)
            return parseFloat(prices[1]) >= parseFloat(minpirce)
          else
            return parseFloat(prices[0]) >= parseFloat(minpirce)
        })
      }

    } else {
      if (minpirce > 0) {
        this.resutlProducts = this.products.filter((a) => {
          let prices = a.price.split('-');
          return parseFloat(prices[0]) >= parseFloat(minpirce)
        })
      }
      if (maxprice > 0) {
        this.resutlProducts = this.products.filter((a) => {
          let prices = a.price.split('-');
          if (prices.length > 1)
            return parseFloat(prices[1]) <= parseFloat(maxprice)
          else
            return parseFloat(prices[0]) <= parseFloat(maxprice)
        })
      }
    }
  }

  setFilters() {
    if (this.filterForm.value.store > 0) {
      this.setStoreToFilter(this.filterForm.value.store)
    }
    this.setPriceFilter(this.filterForm.value.minprice, this.filterForm.value.maxprice)
  }

  resetFilter() {
    this.resutlProducts = this.products;
  }

  setPaging(pagesize: number = 30) {
    let totalstore = this.storeUrls.length
    let totalSum = 0;
    let pagesizePerStore = Math.floor(pagesize / totalstore);
    for (let i = 0; i < totalstore; i++) {
      this.storeUrls[i].pageSize = pagesizePerStore;
      this.storeUrls[i].currentPage = 0;
      this.storeUrls[i].nextPage = 1;
      totalSum += pagesizePerStore;
    }
    totalSum -= pagesizePerStore;
    this.storeUrls[totalstore - 1].pageSize = pagesize - totalSum;
  }

  ngOnInit(): void {
  }
pageSize = 5;
pageNumber = 1;
  getProducts(store: IStore) {
  
    //if (store.nextPage > 0) {
      
      this.http.get<any>(store.siteUrl + 'products.json?limit=' + this.pageSize + '&page=' + this.pageNumber).subscribe((res) => {
        
        for (let j = 0; j < res.products.length; j++) {
          if (res.products) {
            store.currentPage++;
            store.nextPage++
          }
          if (res.products.length < store.pageSize) {
            store.nextPage = 0;
          }
          let prod = res.products[j];
          prod.store = store;
          if (prod.variants) {
            if (prod.variants.length > 1) {
              var minprice = prod.variants.sort((a, b) => { return parseFloat(a.price) - parseFloat(b.price) })[0].price
              var maxprice = prod.variants.sort((a, b) => { return parseFloat(a.price) - parseFloat(b.price) })[prod.variants.length - 1].price
              prod.price = minprice + ' - ' + maxprice;
            } else {
              prod.price = prod.variants[0].price;
            }
          }
          prod.body_html =prod.body_html.replace(/<[^>]*>/g, '');
          this.products.push(prod);
        }
        this.products.sort((a, b) => {
          let day1 = new Date(a.published_at);
          let day2 = new Date(b.published_at);
          return day2.getDate() - day1.getDate()
        })
      })
    //}
  }

  // submit() {

  // }
  resetForm() {
    this.filterForm.reset();
    this.resetFilter()
  }

  onScrollDown(ev: any) {
    
    setTimeout(()=> {
      
      if(this.pageSize < 251){

        this.pageSize += 5;
        this.pageNumber += 1;
        this.papulateProducts();
        this.setFilters();
      }
    }, 1000)
  }

}
