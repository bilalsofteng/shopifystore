import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService {

  apiUrl: string ;//= 'http://webfeedtest1.informanagement.com/api';// = environment.APIEndpoint;
  constructor(private auth : AuthService, @Inject('BASE_URL') private baseUrl: string) {
    this.apiUrl = this.baseUrl + "api";
   }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    debugger
    const token = this.auth.getToken();
    let index = req.url.indexOf(this.apiUrl);
    if (token && index > -1) {
        req = req.clone({
            setHeaders: {
                Authorization: `Bearer ${this.auth.getToken()}`
            }
        });
    }
    return next.handle(req);
}
}
