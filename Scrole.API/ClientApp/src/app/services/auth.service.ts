import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { IChangePassword } from '../models/change-password';
import { ILogin } from '../models/login';
import { IRegister } from '../models/register';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //apiUrl: string = environment.APIEndpoint;
  //apiUrl: string = JSON.parse(localStorage.getItem('apiUrl'));
  apiUrl: string ;
  constructor(private http: HttpClient, public jwtHelper: JwtHelperService,@Inject('BASE_URL') private baseUrl: string) { 
    this.apiUrl = this.baseUrl + "api";
  }

  public register(model: IRegister): Observable<IRegister> {
    return this.http.post<IRegister>(this.apiUrl + '/auth/register', model)
  }

  public login(model: ILogin): Observable<any> {
    return this.http.post<any>(this.apiUrl + '/auth/login', model)
  }
  getProfile(): Observable<IRegister>{
    return this.http.get<IRegister>(this.apiUrl+"/auth/profile")
  }
  changePassword(model: IChangePassword) : Observable<IChangePassword>{
    return this.http.post<IChangePassword>(this.apiUrl+"/auth/ChangePassword", model)
   }

   update(model: IRegister): Observable<any>{
     return this.http.post<any>(this.apiUrl+"/auth/Update", model)
   }

   resetEmailToken(email: string):Observable<string>{
     return this.http.get<string>(this.apiUrl+'/auth/ResetToken?email='+email)
   }

   resetPassword(email: string, password : string, token: string): Observable<string>{
     return this.http.get<any>(this.apiUrl+'/auth/ResetPassword?email='+email+'&token='+token+'&password='+password)
   }

  public isAuthenticated(): boolean {
    const token = this.getToken();
    if(!token){
      return false;
    }
    // Check whether the token is expired and return
    // true or false
    return !this.jwtHelper.isTokenExpired(token);
  }

  public getToken():string{
    return localStorage.getItem('token');
  }

  public logout(){
    localStorage.removeItem('token');
  }
}
