import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { serialize } from 'object-to-formdata';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IStore } from '../models/store';

@Injectable({
  providedIn: 'root'
})
export class StoreService {
  apiUrl: string ;//= environment.APIEndpoint;
  constructor(private http : HttpClient,@Inject('BASE_URL') private baseUrl: string) {
    this.apiUrl = this.baseUrl + "api";
    //localStorage.setItem('apiUrl', JSON.stringify(this.apiUrl));
    
   }

  private options = {
    /**
     * include array indices in FormData keys
     * defaults to false
     */
    indices: true,
   
    /**
     * treat null values like undefined values and ignore them
     * defaults to false
     */
    nullsAsUndefineds: true,
   
    /**
     * convert true or false to 1 or 0 respectively
     * defaults to false
     */
    booleansAsIntegers: false,
   
    /**
     * store arrays even if they're empty
     * defaults to false
     */
    allowEmptyArrays: true,
  };

  public add(model : IStore, file: any):Observable<IStore>{
    let headers = new HttpHeaders();
    //this is the important step. You need to set content type as null
    headers.set('Content-Type', null);
    headers.set('Accept', "multipart/form-data");
    let params = new HttpParams();
    var form_data = new FormData();
    form_data = serialize(model, this.options);
    // form_data = '';
    if(file){
      form_data.append('image', file);
    }
    return this.http.post<IStore>(this.apiUrl+'api/store/create', form_data, { params, headers})
  }

  public update(model : IStore, file: any):Observable<IStore>{
    let headers = new HttpHeaders();
    //this is the important step. You need to set content type as null
    headers.set('Content-Type', null);
    headers.set('Accept', "multipart/form-data");
    let params = new HttpParams();
    var form_data = new FormData();
    form_data = serialize(model, this.options);
    // form_data = '';
    if(file){
      form_data.append('image', file);
    }
    return this.http.post<IStore>(this.apiUrl+'/store/update', form_data, { params, headers})
  }

  public getAll():Observable<Array<IStore>>{
    
    return this.http.get<Array<IStore>>(this.apiUrl+'/store/all')
  }

  public getById(id):Observable<IStore>{
    
    return this.http.get<IStore>(this.apiUrl+'/store/byId?id='+id)
  }

  public getByUser():Observable<Array<IStore>>{
    return this.http.get<Array<IStore>>(this.apiUrl+'/store/byUser')
  }

  public delete(id : number): Observable<number>{
    return this.http.post<number>(this.apiUrl+'/store/delete?id='+id,null)
  }

  public getStores():Observable<Array<IStore>>{
    
    return this.http.get<Array<IStore>>(this.apiUrl+'/stores/getstores')
  }

  public getStoreTypes():Observable<Array<IStore>>{
    
    return this.http.get<Array<IStore>>(this.apiUrl+'/stores/getstoretypes')
  }

  public getStoreByStoreTypeId(storeTypeId:number):Observable<Array<IStore>>{
    debugger
    return this.http.get<Array<IStore>>(this.apiUrl+'/stores/getstoresbystoretypeId/'+storeTypeId)
  }

  public getTrentingStores():Observable<Array<IStore>>{
    
    return this.http.get<Array<IStore>>(this.apiUrl+'/stores/gettrendingstores')
  }

  public getStoreById(id:number):Observable<Array<IStore>>{
    debugger
    return this.http.get<Array<IStore>>(this.apiUrl+'/stores/getstorebyid/'+id)
  }

}
