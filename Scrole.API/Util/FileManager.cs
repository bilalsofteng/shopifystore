﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Scrole.API.Util
{
    public static class FileManager
    {
        public static async Task<string> SaveImage(IFormFile image, string rootDir)
        {
            string uploadfolder = "/uploads/";
            if (!Directory.Exists(rootDir + uploadfolder))
            {
                Directory.CreateDirectory(rootDir + uploadfolder);
            }
            string filePath = uploadfolder + getUniqueName(image.FileName);
            using (Stream fileStream = new FileStream(rootDir + filePath, FileMode.Create))
            {
                await image.CopyToAsync(fileStream);
            }
            return filePath;
        }

        private static string getUniqueName(string fileName)
        {
            var date = DateTime.Now;
            return "" + date.Year + date.Month + date.Day + date.Hour + date.Minute + date.Second + date.Millisecond + date.Ticks + fileName;
        }
    }
}
