﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Scrole.API.Util
{
    public static class EmailManager
    {
        public static void SendEmail(string content, string to, string subjectContent)
        {
            var configurationBuilder = new ConfigurationBuilder();
            var path = Path.Combine(Directory.GetCurrentDirectory(), "appsettings.json");
            configurationBuilder.AddJsonFile(path);

            var root = configurationBuilder.Build();

            var fromAddress = new MailAddress(root.GetSection("smpt").GetSection("from").Value, "Scrolle");
            var toAddress = new MailAddress(to);
            string fromPassword = root.GetSection("smpt").GetSection("password").Value; 
            string subject = subjectContent;
            string body = content;

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }
    }
}
