﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace Scrole.API.Util
{
    public static class IdentityExtension
    {
        public static long GetCustomId(this IIdentity identity)
        {
            long id = 0;
            if (identity != null)
            {
                var claims = identity as ClaimsIdentity;
                if (claims != null)
                {
                    var cid = claims.FindFirst("CustomId").Value;
                    if (!string.IsNullOrEmpty(cid))
                    {
                        id = Convert.ToInt64(cid);
                    }
                }
            }
            return id;
        }

        public static string GetEmail(this IIdentity identity)
        {
            if (identity != null)
            {
                var claims = identity as ClaimsIdentity;
                if (claims != null)
                {
                    var email = claims.FindFirst(ClaimTypes.Email).Value;
                    if (!string.IsNullOrEmpty(email))
                    {
                        return email;
                    }
                }
            }
            return string.Empty; ;
        }
    }
}
