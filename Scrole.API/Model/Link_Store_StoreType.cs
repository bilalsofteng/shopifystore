﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Scrole.API.Model
{
    [Table("Link_Store_StoreTypes")]
    public class Link_Store_StoreType
    {
        [Key]
        public int LinkId { get; set; }
        public int StoreId { get; set; }
        public int StoreTypeId { get; set; }
    }
}
