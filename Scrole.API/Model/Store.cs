﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Scrole.API.Model
{
    [Table("Store")]
    public class Store : BaseEntity
    {
        [Key]
        [Required]

        public string Title { get; set; }
        public string ThumbnailUrl { get; set; }
        public long UserId { get; set; }
        public string SiteUrl { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string  StoreIcon { get; set; }
    }
}
