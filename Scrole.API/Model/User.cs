﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Scrole.API.Model
{
    public class User : IdentityUser
    {
        [Required, StringLength(50)]
        public string FirstName { get; set; }
        [Required, StringLength(50)]
        public string LastName { get; set; }
        [StringLength(500)]
        public string Address { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long CustomId { get; set; }
    }
}
