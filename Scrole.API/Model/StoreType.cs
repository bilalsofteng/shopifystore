﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Scrole.API.Model
{
    [Table("StoreTypes")]
    public class StoreType
    {
        [Key]
        public int StoreTypeID { get; set; }
        public string StoreTypeTitle { get; set; }
        public bool StoreTypeActive { get; set; }
        public DateTime? StoreTypeCreatedDate { get; set; }
        public int? StoreTypeCreatedBy { get; set; }
        public DateTime? StoreTypeModifiedDate { get; set; }
        public int? StoreTypeModifiedBy { get; set; }
        public bool? IsTrending { get; set; }
    }
}
