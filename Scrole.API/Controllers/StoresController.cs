﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Scrole.API.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Scrole.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoresController : ControllerBase
    {

        private DBContext _dbcontext { get; set; }
        public StoresController(DBContext context)
        {
            _dbcontext = context;
        }
        [HttpGet, Route("getstores")]
        public IActionResult GetStores()
        {
            try
            {
                var test = _dbcontext.Stores.ToList();
                var stores = _dbcontext.Stores.Where(x => x.IsActive == true && x.IsDeleted == false).ToList();
                return Ok(stores);
            }
            catch (Exception Ex)
            {

                return Ok(Ex.Message);
            }
        }

        [HttpGet, Route("gettrendingstores")]
        public IActionResult GetTrendingStores()
        {
            try
            {

                var stores = (from store in _dbcontext.Stores
                              join link in _dbcontext.Link_Store_StoreTypes on store.Id equals link.StoreId
                              join storetype in _dbcontext.StoreTypes on link.StoreTypeId equals storetype.StoreTypeID
                              where store.IsActive == true && storetype.IsTrending == true
                              select new Store
                              {
                                  Id = store.Id,
                                  Title = store.Title,
                                  SiteUrl = store.SiteUrl,
                                  ThumbnailUrl = store.ThumbnailUrl,
                                  StoreIcon = store.StoreIcon
                              });
                return Ok(stores);
            }
            catch (Exception Ex)
            {

                return Ok(Ex.Message);
            }
        }

        [HttpGet, Route("getstoretypes")]
        public IActionResult GetStoreTypes()
        {
            try
            {
                var storeTypes = _dbcontext.StoreTypes.Where(x => x.StoreTypeActive == true).ToList();
                return Ok(storeTypes);
            }
            catch (Exception Ex)
            {

                return Ok(Ex.Message);
            }
        }

        [HttpGet, Route("getstoresbystoretypeId/{storeTypeId}")]
        public IActionResult GetStoresbystoreTypeId(int storeTypeId)
        {
            try
            {
                var storebySoreType = (from store in _dbcontext.Stores
                                       join link in _dbcontext.Link_Store_StoreTypes on store.Id equals link.StoreId
                                       join storetype in _dbcontext.StoreTypes on link.StoreTypeId equals storetype.StoreTypeID
                                       where storetype.StoreTypeID == storeTypeId && store.IsActive == true
                                       select new Store
                                       {
                                           Id = store.Id,
                                           Title = store.Title,
                                           SiteUrl = store.SiteUrl,
                                           ThumbnailUrl = store.ThumbnailUrl,
                                           StoreIcon = store.StoreIcon,

                                       });

                return Ok(storebySoreType);
            }
            catch (Exception Ex)
            {

                return Ok(Ex.Message);
            }
        }

        [HttpGet, Route("getstorebyid/{id}")]
        public IActionResult GetStoreById(int id)
        {
            try
            {
                var storebySoreType = _dbcontext.Stores.Where(x => x.Id == id).ToList();
                return Ok(storebySoreType);
            }
            catch (Exception Ex)
            {

                return Ok(Ex.Message);
            }
        }


    }
}
