﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Scrole.API.Util;
using Scrole.BLL.BL;
using Scrole.DAL.Model;

namespace Scrole.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StoreController : ControllerBase
    {
        StoreBL _bl;
        private IWebHostEnvironment _hostingEnvironment;
        public StoreController(IWebHostEnvironment env)
        {
            _bl = new StoreBL();
            _hostingEnvironment = env;
        }
        [HttpGet, Route("all")]
        public async Task<IActionResult> Get()
        {
            var stores = _bl.GetALL();
            return Ok(stores);
        }

        [Authorize, HttpGet, Route("byId")]
        public async Task<IActionResult> GetById(long Id)
        {
            var stores = _bl.GetById(Id);
            return Ok(stores);
        }

        [Authorize, HttpGet, Route("byUser")]
        public async Task<IActionResult> GetByUser()
        {
            var stores = _bl.GetByUser(User.Identity.GetCustomId());
            return Ok(stores);
        }

        [Authorize, HttpPost, Route("create")]
        public async Task<IActionResult> Create([FromForm] Store model, IFormFile image)
        {
            if (image == null)
            {
                return BadRequest();
            }
            
            model.ThumbnailUrl = FileManager.SaveImage(image, _hostingEnvironment.ContentRootPath).Result;
            model.CreatedOn = DateTime.Now;
            model.UserId = User.Identity.GetCustomId();
            model.CreatedBy = User.Identity.GetCustomId();
          //  model = _bl.Add(model);

            return Ok(model);
        }

        [Authorize,HttpPost, Route("update")]
        public async Task<IActionResult> Update([FromForm] Store model, IFormFile image)
        {
            var oldmodel = _bl.GetById(model.Id);
            if (image != null)
            {
                oldmodel.ThumbnailUrl = FileManager.SaveImage(image, _hostingEnvironment.ContentRootPath).Result;
            }

            oldmodel.ModifiedOn = DateTime.Now;
            oldmodel.UserId = User.Identity.GetCustomId();
            oldmodel.ModifiedBy = User.Identity.GetCustomId();
            oldmodel.SiteUrl = model.SiteUrl;
            oldmodel.Title = model.Title;
            oldmodel.Description = model.Description;
            oldmodel = _bl.Update(oldmodel);
            return Ok(oldmodel);
        }

        [Authorize, HttpPost, Route("Delete")]
        public async Task<IActionResult> Delete(long Id)
        {
            var oldmodel = _bl.Delete(Id);
            return Ok(oldmodel);
        }
    }
}
