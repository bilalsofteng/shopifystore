﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Scrole.API.Model;
using Scrole.API.Util;

namespace Scrole.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserManager<User> userManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly IConfiguration _configuration;

        public AuthController(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, IConfiguration configuration)
        {
            this.userManager = userManager;
            this.roleManager = roleManager;
            _configuration = configuration;
        }

        [HttpPost]
        [Route("login")]
        public async Task<IActionResult> Login([FromBody] LoginModel model)
        {
            var user = await userManager.FindByEmailAsync(model.Username);
            if (user != null && await userManager.CheckPasswordAsync(user, model.Password))
            {
                var userRoles = await userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.FirstName+ " "+ user.LastName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("CustomId", user.CustomId.ToString())
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    roles = string.Join(",", userRoles)
                });
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {
            if (!await roleManager.RoleExistsAsync("Admin"))
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            if (!await roleManager.RoleExistsAsync("Store"))
                await roleManager.CreateAsync(new IdentityRole("Store"));
            if (!await roleManager.RoleExistsAsync("Client"))
                await roleManager.CreateAsync(new IdentityRole("Client"));
            //if (!model.IsClient && !model.IsStore)
            //{
            //    return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "Select account type!" });
            //}
            var userExists = await userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
                return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "User already exists!" });

            User user = new User()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.Phone,
                Address = model.Address
            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new { Status = "Error", Message = "User creation failed! Please check user details and try again." });
            await userManager.AddToRoleAsync(user, "Client");

            return Ok(user);
        }

        [HttpGet, Authorize]
        [Route("profile")]
        public async Task<IActionResult> Profile()
        {
            var model = await userManager.FindByEmailAsync(User.Identity.GetEmail());
            RegisterModel userModel = new RegisterModel { Email = model.Email, FirstName = model.FirstName, LastName = model.LastName, Address = model.Address, Phone = model.PhoneNumber };
            return Ok(userModel);
        }

        [HttpPost, Authorize]
        [Route("changepassword")]
        public async Task<IActionResult> ChangePassword(ChangePasswordModel model)
        {
            var userModel = await userManager.FindByEmailAsync(User.Identity.GetEmail());
            var result = await userManager.ChangePasswordAsync(userModel, model.OldPassword, model.NewPassword);
            if (result.Succeeded)
            {
                return Ok();

            }
            return BadRequest(result.Errors);
        }

        [HttpPost]
        [Route("register-admin")]
        public async Task<IActionResult> RegisterAdmin([FromBody] RegisterModel model)
        {
            var userExists = await userManager.FindByNameAsync(model.Email);
            if (userExists != null)
                return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "User already exists!" });

            User user = new User()
            {
                Email = model.Email,
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = model.Email,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.Phone,
                Address = model.Address
            };
            var result = await userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new { Status = "Error", Message = "User creation failed! Please check user details and try again." });

            if (!await roleManager.RoleExistsAsync("Admin"))
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            if (!await roleManager.RoleExistsAsync("Client"))
                await roleManager.CreateAsync(new IdentityRole("Client"));

            if (await roleManager.RoleExistsAsync("Admin"))
            {
                await userManager.AddToRoleAsync(user, "Admin");
            }

            return Ok(new { Status = "Success", Message = "User created successfully!" });
        }
        [HttpGet, Authorize(Roles = "Admin")]
        [Route("Users")]
        public async Task<IActionResult> Users()
        {
            var users = userManager.Users.Select(x => new { FirstName = x.FirstName, Email = x.Email, Id = x.CustomId }).ToList();
            return Ok(users);

        }

        [HttpGet, Route("ResetToken")]
        public async Task<IActionResult> GenrateRestToken(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user != null && user.CustomId > 0)
            {
                var token = await userManager.GeneratePasswordResetTokenAsync(user);
                EmailManager.SendEmail(token, user.Email, "Password Reset Token");
                return Ok(new { Status = "success", Message = "Token sent to email" });
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "User dose not exists!" });
            }

        }

        [HttpGet, Route("ResetPassword")]
        public async Task<IActionResult> RestPassword(string email, string token, string password)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (user != null && user.CustomId > 0)
            {
                token = HttpUtility.UrlDecode(token);
                var us = await userManager.ResetPasswordAsync(user, token, password);
                if (us != null && us.Succeeded)
                {
                    return Ok("Password change success fully!");
                }
                else
                {
                    return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "Something went wrong!" });
                }
            }
            else
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { Status = "Error", Message = "User dose not exists!" });
            }

        }

        [HttpPost, Authorize, Route("Update")]
        public async Task<IActionResult> update(RegisterModel model)
        {
            var us = await userManager.FindByEmailAsync(User.Identity.GetEmail());
            us.Email = model.Email;
            us.Address = model.Address;
            us.UserName = model.Email;
            us.FirstName = model.FirstName;
            us.LastName = model.LastName;
            us.PhoneNumber = model.Phone;

            var up = await userManager.UpdateAsync(us);
            if (up != null && up.Succeeded)
            {
                var user = await userManager.FindByEmailAsync(us.Email);
                var userRoles = await userManager.GetRolesAsync(user);

                var authClaims = new List<Claim>
                {
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.Name, user.FirstName+ " "+ user.LastName),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim("CustomId", user.CustomId.ToString())
                };

                foreach (var userRole in userRoles)
                {
                    authClaims.Add(new Claim(ClaimTypes.Role, userRole));
                }

                var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JWT:Secret"]));

                var token = new JwtSecurityToken(
                    issuer: _configuration["JWT:ValidIssuer"],
                    audience: _configuration["JWT:ValidAudience"],
                    expires: DateTime.Now.AddHours(3),
                    claims: authClaims,
                    signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                    );

                return Ok(new
                {
                    token = new JwtSecurityTokenHandler().WriteToken(token),
                    expiration = token.ValidTo,
                    roles = string.Join(",", userRoles)
                });
            }

            return BadRequest();
        }
    }
}
