﻿using Scrole.DAL.DL;
using Scrole.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Scrole.BLL.BL
{
    public class StoreBL
    {
        StoreDL _dl;
        public StoreBL()
        {
            _dl = new StoreDL();
        }

        public Store Add(Store model)
        {
           model.Id = _dl.Manage("insert", model);
            return model;
        }

        public Store Update(Store model)
        {
            model.Id = _dl.Manage("update", model);
            return model;
        }

        public long Delete(long Id)
        {
            Store model = new Store();
            model.Id = Id;
            return _dl.Manage("delete", model);
        }

        public List<Store> GetALL()
        {
            List<Store> stores = new List<Store>();
            var ds = _dl.Get("all");
            if (ds?.Tables?[0].Rows.Count > 0)
            {
                var dt = ds.Tables[0];
                stores = Mapper(dt);
            }
            return stores;
        }

        public Store GetById(long Id)
        {
            Store store = new Store();
            var ds = _dl.Get("byId", Id:Id);
            if (ds?.Tables?[0].Rows.Count > 0)
            {
                var dt = ds.Tables[0];
                store = Mapper(dt).FirstOrDefault();
            }
            return store;
        }
        public List<Store> GetByUser(long UserId)
        {
            List<Store> stores = new List<Store>();
            var ds = _dl.Get("byUser", UserId:UserId);
            if (ds?.Tables?[0].Rows.Count > 0)
            {
                var dt = ds.Tables[0];
                stores = Mapper(dt);
            }
            return stores;
        }

        private List<Store> Mapper(DataTable dt)
        {
            List<Store> stores = new List<Store>();
            if (dt?.Rows.Count > 0)
            {
                stores = dt.AsEnumerable().Select(col => new Store()
                {
                    Id = col.Field<long>("Id"),
                    Title = col.Field<string>("Title"),
                    Description = col.Field<string>("Description"),
                    SiteUrl = col.Field<string>("SiteUrl"),
                    ThumbnailUrl = col.Field<string>("ThumbnailUrl"),
                    IsActive = col.Field<bool>("IsActive"),
                    CreatedBy = col.Field<long>("CreatedBy"),
                    CreatedOn = col.Field<DateTime>("CreatedOn"),
                    ModifiedBy = col.Field<long?>("ModifiedBy"),
                    ModifiedOn = col.Field<DateTime?>("ModifiedOn"),
                    IsDeleted = col.Field<bool>("IsDeleted"),
                }).ToList();
            }
            return stores;
        }
    }
}
